\begin{Verbatim}[commandchars=\\\{\}]
\PYG{c+c1}{// Integrate angular velocity to get angle}
\PYG{n}{currentAngle} \PYG{o}{+=} \PYG{n}{gyroVals}\PYG{p}{[}\PYG{l+m+mi}{2}\PYG{p}{]} \PYG{o}{*} \PYG{n}{ACCEL\PYGZus{}READ\PYGZus{}PERIOD} \PYG{o}{*} \PYG{p}{(}\PYG{n}{M\PYGZus{}PI}\PYG{o}{/}\PYG{l+m+mi}{180}\PYG{p}{);}

\PYG{c+c1}{// Convert acceleration to mm/s\PYGZca{}2}
\PYG{n}{accelVals}\PYG{p}{[}\PYG{l+m+mi}{0}\PYG{p}{]} \PYG{o}{*=} \PYG{l+m+mf}{9.81}\PYG{p}{;}
\PYG{n}{accelVals}\PYG{p}{[}\PYG{l+m+mi}{1}\PYG{p}{]} \PYG{o}{*=} \PYG{l+m+mf}{9.81}\PYG{p}{;}

\PYG{c+c1}{// Correct raw accel axis for centrifugal effects}
\PYG{k+kt}{double} \PYG{n}{omega} \PYG{o}{=} \PYG{n}{gyroVals}\PYG{p}{[}\PYG{l+m+mi}{2}\PYG{p}{]} \PYG{o}{*} \PYG{n}{ACCEL\PYGZus{}READ\PYGZus{}PERIOD}\PYG{p}{;}
\PYG{n}{accelVals}\PYG{p}{[}\PYG{l+m+mi}{0}\PYG{p}{]} \PYG{o}{+=} \PYG{n}{rAccelCorrect}\PYG{o}{*}\PYG{n}{cos}\PYG{p}{(}\PYG{n}{alphaAccelCorrect}\PYG{p}{)}\PYG{o}{*}\PYG{n}{omega}\PYG{o}{*}\PYG{n}{omega}\PYG{p}{;}
\PYG{n}{accelVals}\PYG{p}{[}\PYG{l+m+mi}{1}\PYG{p}{]} \PYG{o}{+=} \PYG{n}{rAccelCorrect}\PYG{o}{*}\PYG{n}{sin}\PYG{p}{(}\PYG{n}{alphaAccelCorrect}\PYG{p}{)}\PYG{o}{*}\PYG{n}{omega}\PYG{o}{*}\PYG{n}{omega}\PYG{p}{;}

\PYG{c+c1}{// Convert to true x and y}
\PYG{k+kt}{double} \PYG{n}{trueAccel}\PYG{p}{[}\PYG{l+m+mi}{2}\PYG{p}{];}
\PYG{n}{trueAccel}\PYG{p}{[}\PYG{l+m+mi}{0}\PYG{p}{]} \PYG{o}{=} \PYG{o}{\PYGZhy{}}\PYG{n}{accelVals}\PYG{p}{[}\PYG{l+m+mi}{0}\PYG{p}{]}\PYG{o}{*}\PYG{n}{sin}\PYG{p}{(}\PYG{n}{currentAngle}\PYG{p}{)} \PYG{o}{\PYGZhy{}} \PYG{n}{accelVals}\PYG{p}{[}\PYG{l+m+mi}{1}\PYG{p}{]}\PYG{o}{*}\PYG{n}{cos}\PYG{p}{(}\PYG{n}{currentAngle}\PYG{p}{);}
\PYG{n}{trueAccel}\PYG{p}{[}\PYG{l+m+mi}{1}\PYG{p}{]} \PYG{o}{=} \PYG{n}{accelVals}\PYG{p}{[}\PYG{l+m+mi}{0}\PYG{p}{]}\PYG{o}{*}\PYG{n}{cos}\PYG{p}{(}\PYG{n}{currentAngle}\PYG{p}{)} \PYG{o}{\PYGZhy{}} \PYG{n}{accelVals}\PYG{p}{[}\PYG{l+m+mi}{1}\PYG{p}{]}\PYG{o}{*}\PYG{n}{sin}\PYG{p}{(}\PYG{n}{currentAngle}\PYG{p}{);}

\PYG{c+c1}{// Integrate acceleration to get velocity and position}
\PYG{k+kt}{int} \PYG{n}{i}\PYG{p}{;}
\PYG{k}{for} \PYG{p}{(}\PYG{n}{i} \PYG{o}{=} \PYG{l+m+mi}{0}\PYG{p}{;} \PYG{n}{i} \PYG{o}{\PYGZlt{}} \PYG{l+m+mi}{2}\PYG{p}{;} \PYG{n}{i}\PYG{o}{++}\PYG{p}{)} \PYG{p}{\PYGZob{}} 
    \PYG{n}{currentVelocity}\PYG{p}{[}\PYG{n}{i}\PYG{p}{]} \PYG{o}{+=} \PYG{n}{trueAccel}\PYG{p}{[}\PYG{n}{i}\PYG{p}{]} \PYG{o}{*} \PYG{n}{ACCEL\PYGZus{}READ\PYGZus{}PERIOD}\PYG{p}{;}
    \PYG{n}{currentPos}\PYG{p}{[}\PYG{n}{i}\PYG{p}{]} \PYG{o}{+=} \PYG{n}{currentVelocity}\PYG{p}{[}\PYG{n}{i}\PYG{p}{]} \PYG{o}{*} \PYG{n}{ACCEL\PYGZus{}READ\PYGZus{}PERIOD}\PYG{p}{;}
\PYG{p}{\PYGZcb{}}
\end{Verbatim}
