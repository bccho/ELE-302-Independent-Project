# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pi/ele302/lib/pixy/src/common/src/chirp.cpp" "/home/pi/ele302/lib/pixy/build/libpixyusb/CMakeFiles/pixyusb.dir/home/pi/ele302/lib/pixy/src/common/src/chirp.cpp.o"
  "/home/pi/ele302/lib/pixy/src/host/libpixyusb/src/chirpreceiver.cpp" "/home/pi/ele302/lib/pixy/build/libpixyusb/CMakeFiles/pixyusb.dir/src/chirpreceiver.cpp.o"
  "/home/pi/ele302/lib/pixy/src/host/libpixyusb/src/pixy.cpp" "/home/pi/ele302/lib/pixy/build/libpixyusb/CMakeFiles/pixyusb.dir/src/pixy.cpp.o"
  "/home/pi/ele302/lib/pixy/src/host/libpixyusb/src/pixyinterpreter.cpp" "/home/pi/ele302/lib/pixy/build/libpixyusb/CMakeFiles/pixyusb.dir/src/pixyinterpreter.cpp.o"
  "/home/pi/ele302/lib/pixy/src/host/libpixyusb/src/usblink.cpp" "/home/pi/ele302/lib/pixy/build/libpixyusb/CMakeFiles/pixyusb.dir/src/usblink.cpp.o"
  "/home/pi/ele302/lib/pixy/src/host/libpixyusb/src/utils/timer.cpp" "/home/pi/ele302/lib/pixy/build/libpixyusb/CMakeFiles/pixyusb.dir/src/utils/timer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "__LIBPIXY_VERSION__=\"0.4\""
  "__LINUX__"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/pi/ele302/lib/pixy/src/host/libpixyusb/src"
  "/home/pi/ele302/lib/pixy/src/host/libpixyusb/src/util"
  "/home/pi/ele302/lib/pixy/src/host/libpixyusb/include"
  "/home/pi/ele302/lib/pixy/src/host/libpixyusb/../../common/inc"
  "/usr/include/libusb-1.0"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
